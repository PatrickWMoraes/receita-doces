

Torta de Frango de Liquidificador
Ingredientes:
3 ovos
1 xícara (chá) de óleo
1 e 1/2 xícaras (chá) de leite
2 colheres (sopa) bem cheias de queijo ralado
1 colher (café) de sal
2 xícaras (chá) de farinha de trigo
1 colher (sopa) fermento em pó
450g de frango desfiado e temperado a gosto (VEJA COMO FAZER O FRANGO)

Modo de Preparo:
No liquidificador, coloque os ovos, o óleo, o leite, o queijo ralado, o sal e a farinha de trigo.
DICA: Coloque sempre os ingredientes líquidos primeiro.
Bata tudo por aproximadamente 2 minutos.
E seja feliz 